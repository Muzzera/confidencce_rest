let createError = require('http-errors');
let db = require('./db/db');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

// Reserved for Graphql
let indexRouter = require('./routes/index');
let usersRouter = require('./model/user/userRoutes');
let planosRouter = require('./model/planos/planosRoutes');
let midiasRouter = require('./model/midias/midiasRoutes');
let scortRoutes = require('./model/scorts/scortsRoutes');
let scort_appearanceRoutes = require('./model/scort_appearance/scort_appearanceRoutes');

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/planos', planosRouter);
app.use('/midias', midiasRouter);
app.use('/scorts', scortRoutes);
app.use('/appearance', scort_appearanceRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});




// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
