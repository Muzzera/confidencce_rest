// Var declarations
let mongoose = require('mongoose');
let dbURI = 'mongodb://confidencce:leb0961@ds347917.mlab.com:47917/confidencce';

// DB Stablishing Connection 
mongoose.connect(dbURI);

// DB on Connected Event
mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to ' + dbURI);
});

// DB on Errors Events
mongoose.connection.on('error',function (err) {
    console.log('Mongoose connection error: ' + err);
});

// // DB on Closed Event
// mongoose.connection.close(function () {
//     console.log('Mongoose default connection closed');
// });

// DB On App termination Process Event
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
      console.log('Mongoose disconnected through app termination');
      process.exit(0);
    });
});