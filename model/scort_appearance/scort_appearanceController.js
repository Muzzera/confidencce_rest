var scort_appearanceModel = require('./scort_appearanceModel.js');

/**
 * scort_appearanceController.js
 *
 * @description :: Server-side logic for managing scort_appearances.
 */
module.exports = {

    /**
     * scort_appearanceController.list()
     */
    list: function (req, res) {
        scort_appearanceModel.find(function (err, scort_appearances) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scort_appearance.',
                    error: err
                });
            }
            return res.json(scort_appearances);
        });
    },

    /**
     * scort_appearanceController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        scort_appearanceModel.findOne({_id: id}, function (err, scort_appearance) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scort_appearance.',
                    error: err
                });
            }
            if (!scort_appearance) {
                return res.status(404).json({
                    message: 'No such scort_appearance'
                });
            }
            return res.json(scort_appearance);
        });
    },

    /**
     * scort_appearanceController.create()
     */
    create: function (req, res) {
        var scort_appearance = new scort_appearanceModel({
            scort_id : req.body.scort_id,
			hair : req.body.hair,
			eyes : req.body.eyes,
			skin : req.body.skin,
			bust : req.body.bust,
			butt : req.body.butt,
            depilation : req.body.depilation,
            created_at : Date.now()

        });

        scort_appearance.save(function (err, scort_appearance) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating scort_appearance',
                    error: err
                });
            }
            return res.status(201).json(scort_appearance);
        });
    },

    /**
     * scort_appearanceController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        scort_appearanceModel.findOne({_id: id}, function (err, scort_appearance) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scort_appearance',
                    error: err
                });
            }
            if (!scort_appearance) {
                return res.status(404).json({
                    message: 'No such scort_appearance'
                });
            }

            scort_appearance.scort_id = req.body.scort_id ? req.body.scort_id : scort_appearance.scort_id;
            scort_appearance.hair = req.body.hair ? req.body.hair : scort_appearance.hair;
			scort_appearance.eyes = req.body.eyes ? req.body.eyes : scort_appearance.eyes;
			scort_appearance.skin = req.body.skin ? req.body.skin : scort_appearance.skin;
			scort_appearance.bust = req.body.bust ? req.body.bust : scort_appearance.bust;
			scort_appearance.butt = req.body.butt ? req.body.butt : scort_appearance.butt;
			scort_appearance.depilation = req.body.depilation ? req.body.depilation : scort_appearance.depilation;
			
            scort_appearance.save(function (err, scort_appearance) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating scort_appearance.',
                        error: err
                    });
                }

                return res.json(scort_appearance);
            });
        });
    },

    /**
     * scort_appearanceController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        scort_appearanceModel.findByIdAndRemove(id, function (err, scort_appearance) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the scort_appearance.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
