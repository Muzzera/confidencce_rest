var express = require('express');
var router = express.Router();
var scort_appearanceController = require('./scort_appearanceController.js');

/*
 * GET
 */
router.get('/', scort_appearanceController.list);

/*
 * GET
 */
router.get('/:id', scort_appearanceController.show);

/*
 * POST
 */
router.post('/', scort_appearanceController.create);

/*
 * PUT
 */
router.put('/:id', scort_appearanceController.update);

/*
 * DELETE
 */
router.delete('/:id', scort_appearanceController.remove);

module.exports = router;
