var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var scort_appearanceSchema = new Schema({
	'scort_id' : {
		type: Schema.Types.ObjectId,
		ref: 'scorts'
    },
	"hair" : {
        type: String,
        required: true
    },
    "eyes" : {
        type: String,
        required: true
    },
    "skin" : {
        type: String,
        required: true
    },
    "bust" : {
        type: String,
        required: true
    },
    "butt" : {
        type: String,
        required: true
    },
    "depilation" : {
        type: String
    },
	'created_at' : Date
});

module.exports = mongoose.model('scort_appearance', scort_appearanceSchema);
