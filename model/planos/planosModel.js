var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var planosSchema = new Schema({
	'title' : {
		type:String,
		required: true,
		unique: true
	},
	'status' : {
		type: Boolean,
		default: true
	},
	'created_at' : {
		type: Date,
		default: Date.now
	},
	'cost' : {
		type: [Number],
		required: true
	},
	'period' : {
		type: [Number],
		required: true,
		min: 1
	},
	'max_image' : {
		type: Number,
		required: true,
		min: 1,
		default: 1
	},
	'max_stories' : {
		type: Number,
		required: true,
		min: 1,
		default: 1
	},
	'edited_at' : {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('planos', planosSchema);
