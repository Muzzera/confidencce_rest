var express = require('express');
var router = express.Router();
var planosController = require('./planosController.js');

/*
 * GET
 */
router.get('/', planosController.list);

/*
 * GET
 */
router.get('/:id', planosController.show);

/*
 * POST
 */
router.post('/', planosController.create);

/*
 * PUT
 */
router.put('/:id', planosController.update);

/*
 * DELETE
 */
router.delete('/:id', planosController.remove);

module.exports = router;
