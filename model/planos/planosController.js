var planosModel = require('./planosModel.js');

/**
 * planosController.js
 *
 * @description :: Server-side logic for managing planos.
 */
module.exports = {

    /**
     * planosController.list()
     */
    list: function (req, res) {
        planosModel.find(function (err, planoss) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting planos.',
                    error: err
                });
            }
            return res.json(planoss);
        });
    },

    /**
     * planosController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        planosModel.findOne({_id: id}, function (err, planos) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting planos.',
                    error: err
                });
            }
            if (!planos) {
                return res.status(404).json({
                    message: 'No such planos'
                });
            }
            return res.json(planos);
        });
    },

    /**
     * planosController.create()
     */
    create: function (req, res) {
        var planos = new planosModel({
			title : req.body.title,
			status : req.body.status,
			created_at : Date.now(),
			cost : req.body.cost,
			period : req.body.period,
			max_image : req.body.max_image,
			max_stories : req.body.max_stories,
			edited_at : Date.now()
        });

        planos.save(function (err, planos) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating planos',
                    error: err
                });
            }
            return res.status(201).json(planos);
        });
    },

    /**
     * planosController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        planosModel.findOne({_id: id}, function (err, planos) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting planos',
                    error: err
                });
            }
            if (!planos) {
                return res.status(404).json({
                    message: 'No such planos'
                });
            }

            planos.title = req.body.title ? req.body.title : planos.title;
			planos.status = req.body.status ? req.body.status : planos.status;
			planos.cost = req.body.cost ? req.body.cost : planos.cost;
			planos.period = req.body.period ? req.body.period : planos.period;
			planos.max_image = req.body.max_image ? req.body.max_image : planos.max_image;
			planos.max_stories = req.body.max_stories ? req.body.max_stories : planos.max_stories;
			planos.edited_at = Date.now();
			
            planos.save(function (err, planos) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating planos.',
                        error: err
                    });
                }

                return res.json(planos);
            });
        });
    },

    /**
     * planosController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        planosModel.findByIdAndRemove(id, function (err, planos) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the planos.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
