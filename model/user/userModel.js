var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var userSchema = new Schema({
	'first_name' : {
		type: String,
		required: true
	},
	'last_name' : {
		type: String,
		required: true
	},
	'email' : {
		type: String,
		required: true,
		unique: true
	},
	'fone_number' : {
		type: String,
		required: true
	},
	'status' : Boolean,
	'level' : {
		type: Array
	},
	'created_at' : Date
});

module.exports = mongoose.model('user', userSchema);
