var scortsModel = require('./scortsModel.js');

/**
 * scortsController.js
 *
 * @description :: Server-side logic for managing scortss.
 */
module.exports = {

    /**
     * scortsController.list()
     */
    list: function (req, res) {
        scortsModel.find(function (err, scortss) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scorts.',
                    error: err
                });
            }
            return res.json(scortss);
        });
    },

    /**
     * scortsController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        scortsModel.findOne({_id: id}, function (err, scorts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scorts.',
                    error: err
                });
            }
            if (!scorts) {
                return res.status(404).json({
                    message: 'No such scorts'
                });
            }
            return res.json(scorts);
        });
    },

    /**
     * scortsController.create()
     */
    create: function (req, res) {
        var scorts = new scortsModel({
			first_name : req.body.first_name,
			last_name : req.body.last_name,
			image_profile : req.body.image_profile,
			login : req.body.login,
			email : req.body.email,
			cpf : req.body.cpf,
			birthdate : req.body.birthdate,
			nationality : req.body.nationality,
			uf : req.body.uf,
			city : req.body.city,
			phone : req.body.phone,
			bio : req.body.bio,
			password : req.body.password,
			status : req.body.status,
			validity : req.body.validity,
			created_at : Date.now(),
			edited_at : req.body.edited_at,
			schedule : req.body.schedule,
			services : req.body.services,
			plano : req.body.plano,
			appearance : req.body.appearance,
			misc : req.body.misc

        });
        console.log(scorts)
        scorts.save(function (err, scorts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating scorts',
                    error: err
                });
            }
            return res.status(201).json(scorts);
        });
    },

    /**
     * scortsController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        scortsModel.findOne({_id: id}, function (err, scorts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting scorts',
                    error: err
                });
            }
            if (!scorts) {
                return res.status(404).json({
                    message: 'No such scorts'
                });
            }

            scorts.first_name = req.body.first_name ? req.body.first_name : scorts.first_name;
			scorts.last_name = req.body.last_name ? req.body.last_name : scorts.last_name;
			scorts.image_profile = req.body.image_profile ? req.body.image_profile : scorts.image_profile;
			scorts.login = req.body.login ? req.body.login : scorts.login;
			scorts.email = req.body.email ? req.body.email : scorts.email;
			scorts.cpf = req.body.cpf ? req.body.cpf : scorts.cpf;
			scorts.birthdate = req.body.birthdate ? req.body.birthdate : scorts.birthdate;
			scorts.nationality = req.body.nationality ? req.body.nationality : scorts.nationality;
			scorts.uf = req.body.uf ? req.body.uf : scorts.uf;
			scorts.city = req.body.city ? req.body.city : scorts.city;
			scorts.phone = req.body.phone ? req.body.phone : scorts.phone;
			scorts.bio = req.body.bio ? req.body.bio : scorts.bio;
			scorts.password = req.body.password ? req.body.password : scorts.password;
			scorts.status = req.body.status ? req.body.status : scorts.status;
			scorts.validity = req.body.validity ? req.body.validity : scorts.validity;
			// scorts.created_at = req.body.created_at ? req.body.created_at : scorts.created_at;
			scorts.edited_at = req.body.edited_at ? Date.now() : Date.now();
			scorts.schedule = req.body.schedule ? req.body.schedule : scorts.schedule;
			scorts.services = req.body.services ? req.body.services : scorts.services;
			// scorts.plano = req.body.plano ? req.body.plano : scorts.plano;
			// scorts.appearance = req.body.appearance ? req.body.appearance : scorts.appearance;
			// scorts.misc = req.body.misc ? req.body.misc : scorts.misc;
			
            scorts.save(function (err, scorts) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating scorts.',
                        error: err
                    });
                }

                return res.json(scorts);
            });
        });
    },

    /**
     * scortsController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        scortsModel.findByIdAndRemove(id, function (err, scorts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the scorts.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
