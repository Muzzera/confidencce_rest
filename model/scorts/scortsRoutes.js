var express = require('express');
var router = express.Router();
var scortsController = require('./scortsController.js');

/*
 * GET
 */
router.get('/', scortsController.list);

/*
 * GET
 */
router.get('/:id', scortsController.show);

/*
 * POST
 */
router.post('/', scortsController.create);

/*
 * PUT
 */
router.put('/:id', scortsController.update);

/*
 * DELETE
 */
router.delete('/:id', scortsController.remove);

module.exports = router;
