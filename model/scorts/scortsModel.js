var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var scortsSchema = new Schema({
	"first_name" : {
        type: String,
        required: true
    },
    "last_name" : {
        type: String,
        required: true
    },
    "image_profile" : {
        type: String,
        required: true
    },
    "login" : {
        type: String,
        required: true,
        unique: true
    },
    "email" : {
        type: String,
        required: true,
        unique: true
    },
    "cpf" : {
        type: String,
        required: true,
        unique: true
    },
    "birthdate" : {
        type: Date,
        required: true
    },
    "nationality" : {
        type: String,
        required: true
    },
    "uf" : {
        type: String,
        required: true
    },
    "city" : {
        type: String,
        required: true
    },
    "phone" : {
        type: String,
        required: true,
        unique: true
    },
    "bio" : {
        type: String,
        required: true
    },
    "password" : {
        type: String,
        required: true
    },
    "status" : {
        type : Boolean,
        default : true
    },
    "validity" : {
        type : Date,
        required: true
    },
    "created_at" : {
        type : Date
    },
    "edited_at" : {
        type : Date
    },
    "schedule" : {
        type: Array,
        required: true
    },
    "services" : {
        type: Array
    },
	'plano' : {
	 	type: String,
	 	required: true
	},
	'appearance' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'scort_appearance'
	},
	'misc' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'scort_misc'
	}
});

module.exports = mongoose.model('scorts', scortsSchema);
