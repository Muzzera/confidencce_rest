var midiasModel = require('./midiasModel.js');

/**
 * midiasController.js
 *
 * @description :: Server-side logic for managing midiass.
 */
module.exports = {

    /**
     * midiasController.list()
     */
    list: function (req, res) {
        midiasModel.find(function (err, midiass) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting midias.',
                    error: err
                });
            }
            return res.json(midiass);
        });
    },

    /**
     * midiasController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        midiasModel.findOne({_id: id}, function (err, midias) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting midias.',
                    error: err
                });
            }
            if (!midias) {
                return res.status(404).json({
                    message: 'No such midias'
                });
            }
            return res.json(midias);
        });
    },

    /**
     * midiasController.create()
     */
    create: function (req, res) {
        var midias = new midiasModel({
			file_name : req.body.file_name,
			mime_type : req.body.mime_type,
			scort_id : req.body.scort_id,
			status : req.body.status,
			created_at : Date.now()

        });

        midias.save(function (err, midias) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating midias',
                    error: err
                });
            }
            return res.status(201).json(midias);
        });
    },

    /**
     * midiasController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        midiasModel.findOne({_id: id}, function (err, midias) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting midias',
                    error: err
                });
            }
            if (!midias) {
                return res.status(404).json({
                    message: 'No such midias'
                });
            }

            midias.file_name = req.body.file_name ? req.body.file_name : midias.file_name;
			midias.mime_type = req.body.mime_type ? req.body.mime_type : midias.mime_type;
			midias.scort_id = req.body.scort_id ? req.body.scort_id : midias.scort_id;
			midias.status = req.body.status ? req.body.status : midias.status;
			midias.created_at = Date.now();
			
            midias.save(function (err, midias) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating midias.',
                        error: err
                    });
                }

                return res.json(midias);
            });
        });
    },

    /**
     * midiasController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        midiasModel.findByIdAndRemove(id, function (err, midias) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the midias.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
