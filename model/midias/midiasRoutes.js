var express = require('express');
var router = express.Router();
var midiasController = require('./midiasController.js');

/*
 * GET
 */
router.get('/', midiasController.list);

/*
 * GET
 */
router.get('/:id', midiasController.show);

/*
 * POST
 */
router.post('/', midiasController.create);

/*
 * PUT
 */
router.put('/:id', midiasController.update);

/*
 * DELETE
 */
router.delete('/:id', midiasController.remove);

module.exports = router;
