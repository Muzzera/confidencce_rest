var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var midiasSchema = new Schema({
	'file_name' : {
		type: String,
		required: true
	},
	'mime_type' : {
		type: String,
		required: true
	},
	'scort_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'scorts'
	},
	'status' : {
		type: Boolean,
		default: false
	},
	'created_at' : {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('midias', midiasSchema);